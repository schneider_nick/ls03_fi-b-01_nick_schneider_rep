package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JEditorPane;
import javax.swing.JTextArea;
import javax.swing.DropMode;
import java.awt.Font;
import javax.swing.SwingConstants;

public class MyGui extends JFrame {

	private JPanel contentPane;
	private JTextField txtHierBitteText;
	private JLabel lblTopText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MyGui frame = new MyGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MyGui() {
		setResizable(false);
		setTitle("Form_aendern_NS");
		this.lblTopText = new JLabel();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 430, 650);
		contentPane = new JPanel();
		contentPane.setToolTipText("");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		this.lblTopText.setText("Hier bitte Text eingeben");
		this.lblTopText.setBackground(new Color(0, 0, 0, 0));
		this.lblTopText.setHorizontalAlignment(SwingConstants.CENTER);
		this.lblTopText.setBounds(10, 11, 386, 65);
		contentPane.add(lblTopText);

		JLabel lblAufgabe1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabe1.setBounds(10, 87, 321, 14);
		contentPane.add(lblAufgabe1);

		JButton btnRed = new JButton("Rot");
		btnRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonRed_clicked();
			}
		});
		btnRed.setBounds(20, 112, 113, 23);
		contentPane.add(btnRed);

		JButton btnGreen = new JButton("Gr\u00FCn");
		btnGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGreen_clicked();
			}
		});
		btnGreen.setBounds(143, 112, 120, 23);
		contentPane.add(btnGreen);

		JButton btnBlue = new JButton("Blau");
		btnBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonBlue_clicked();
			}
		});
		btnBlue.setBounds(273, 112, 123, 23);
		contentPane.add(btnBlue);

		JButton btnYellow = new JButton("Gelb");
		btnYellow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonYellow_clicked();
			}
		});
		btnYellow.setBounds(20, 146, 113, 23);
		contentPane.add(btnYellow);

		JButton btnGrey = new JButton("Standardfarbe");
		btnGrey.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGrey_clicked();
			}
		});
		btnGrey.setBounds(143, 146, 120, 23);
		contentPane.add(btnGrey);

		JButton btnCustomColor = new JButton("Farbe w\u00E4hlen");
		btnCustomColor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCustom_clicked();
			}
		});
		btnCustomColor.setBounds(273, 146, 123, 23);
		contentPane.add(btnCustomColor);

		JLabel lblAufgabe2 = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabe2.setBounds(10, 180, 336, 14);
		contentPane.add(lblAufgabe2);

		JButton btnArial = new JButton("Arial");
		btnArial.setFont(new Font("Arial", Font.PLAIN, 11));
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonArial_clicked();
			}
		});
		btnArial.setBounds(20, 205, 113, 23);
		contentPane.add(btnArial);

		JButton btnComicSans = new JButton("Comic Sans MS");
		btnComicSans.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonComicSans_clicked();
			}
		});
		btnComicSans.setFont(new Font("Comic Sans MS", Font.PLAIN, 11));
		btnComicSans.setBounds(143, 205, 120, 23);
		contentPane.add(btnComicSans);

		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCourierNew_clicked();
			}
		});
		btnCourierNew.setFont(new Font("Courier New", Font.PLAIN, 11));
		btnCourierNew.setBounds(273, 205, 123, 23);
		contentPane.add(btnCourierNew);

		txtHierBitteText = new JTextField();
		txtHierBitteText.setForeground(Color.GRAY);
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setToolTipText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(20, 243, 376, 21);
		contentPane.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);

		JButton btnSchreiben = new JButton("Ins Label schreiben");
		btnSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonSchreiben_clicked();
			}
		});
		btnSchreiben.setBounds(20, 275, 178, 23);
		contentPane.add(btnSchreiben);

		JButton btnDelete = new JButton("Text im Label l\u00F6schen");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonDelete_clicked();
			}
		});
		btnDelete.setBounds(208, 275, 188, 23);
		contentPane.add(btnDelete);

		JLabel lblAufgabe3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabe3.setBounds(10, 309, 387, 14);
		contentPane.add(lblAufgabe3);

		JButton btnTextRot = new JButton("Rot");
		btnTextRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonTextRot_clicked();
			}
		});
		btnTextRot.setBounds(20, 334, 113, 23);
		contentPane.add(btnTextRot);

		JButton btnTextBlau = new JButton("Blau");
		btnTextBlau.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnTextBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonTextBlau_clicked();
			}
		});
		btnTextBlau.setBounds(143, 334, 120, 23);
		contentPane.add(btnTextBlau);

		JButton btnTextSchwarz = new JButton("Schwarz");
		btnTextSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonTextSchwarz_clicked();
			}
		});
		btnTextSchwarz.setBounds(273, 334, 123, 23);
		contentPane.add(btnTextSchwarz);

		JLabel lblAufgabe4 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblAufgabe4.setBounds(10, 368, 386, 14);
		contentPane.add(lblAufgabe4);

		JButton btnSizePlus = new JButton("+");
		btnSizePlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonSizePlus_clicked();
			}
		});
		btnSizePlus.setBounds(20, 393, 178, 23);
		contentPane.add(btnSizePlus);

		JButton btnSizeMinus = new JButton("-");
		btnSizeMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonSizeMinus_clicked();
			}
		});
		btnSizeMinus.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnSizeMinus.setBounds(208, 393, 188, 23);
		contentPane.add(btnSizeMinus);

		JLabel lblAufgabe5 = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabe5.setBounds(10, 427, 386, 14);
		contentPane.add(lblAufgabe5);

		JButton btnLinks = new JButton("linksb\u00FCndig");
		btnLinks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonLinks_clicked();
			}
		});
		btnLinks.setBounds(20, 458, 113, 23);
		contentPane.add(btnLinks);

		JButton btnCenter = new JButton("zentriert");
		btnCenter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonCenter_clicked();
			}
		});
		btnCenter.setBounds(143, 458, 120, 23);
		contentPane.add(btnCenter);

		JButton btnRechts = new JButton("rechtsb\u00FCndig");
		btnRechts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonRechts_clicked();
			}
		});
		btnRechts.setBounds(273, 458, 123, 23);
		contentPane.add(btnRechts);

		JLabel lblAufgabe6 = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabe6.setBounds(10, 499, 376, 14);
		contentPane.add(lblAufgabe6);

		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonExit_clicked();
			}
		});
		btnExit.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnExit.setBounds(20, 527, 376, 73);
		contentPane.add(btnExit);
	}

	protected void buttonExit_clicked() {
		System.exit(0);

	}

	protected void buttonRechts_clicked() {
		this.lblTopText.setHorizontalAlignment(SwingConstants.RIGHT);

	}

	protected void buttonCenter_clicked() {
		this.lblTopText.setHorizontalAlignment(SwingConstants.CENTER);

	}

	protected void buttonLinks_clicked() {
		this.lblTopText.setHorizontalAlignment(SwingConstants.LEFT);

	}

	protected void buttonSizeMinus_clicked() {
		int S = this.lblTopText.getFont().getSize();
		S--;
		this.lblTopText.setFont(new Font(this.lblTopText.getFont().getName(), 0, S));

	}

	protected void buttonSizePlus_clicked() {
		int S = this.lblTopText.getFont().getSize();
		S++;
		this.lblTopText.setFont(new Font(this.lblTopText.getFont().getName(), 0, S));

	}

	protected void buttonTextSchwarz_clicked() {
		lblTopText.setForeground(Color.BLACK);

	}

	protected void buttonTextBlau_clicked() {
		lblTopText.setForeground(Color.BLUE);

	}

	protected void buttonTextRot_clicked() {
		lblTopText.setForeground(Color.RED);

	}

	protected void buttonDelete_clicked() {
		this.lblTopText.setText("");

	}

	protected void buttonSchreiben_clicked() {
		String Text = this.txtHierBitteText.getText();
		this.lblTopText.setText(Text);

	}

	protected void buttonCourierNew_clicked() {
		this.lblTopText.setFont(new Font("Courier New", 0, this.lblTopText.getFont().getSize()));

	}

	protected void buttonComicSans_clicked() {
		this.lblTopText.setFont(new Font("Comic Sans MS", 0, this.lblTopText.getFont().getSize()));

	}

	protected void buttonArial_clicked() {
		this.lblTopText.setFont(new Font("Arial", 0, this.lblTopText.getFont().getSize()));
	}

	protected void buttonCustom_clicked() {
		Color ausgewaehlteFarbe = JColorChooser.showDialog(null, "Color Picker", null);
		this.contentPane.setBackground(ausgewaehlteFarbe);

	}

	protected void buttonGrey_clicked() {
		Color standard = new Color(240, 240, 240, 255);
		this.contentPane.setBackground(standard);

	}

	protected void buttonYellow_clicked() {
		this.contentPane.setBackground(Color.YELLOW);

	}

	protected void buttonBlue_clicked() {
		this.contentPane.setBackground(Color.BLUE);

	}

	protected void buttonGreen_clicked() {
		this.contentPane.setBackground(Color.GREEN);

	}

	protected void buttonRed_clicked() {
		this.contentPane.setBackground(Color.RED);
	}
}
